/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusChangeListener;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.graphic.core.RGraphic;
import org.eclipse.statet.rj.ts.core.RTool;


/**
 * R graphic for Eclipse based clients.
 * 
 * <h4>Coordinate systems:</h4>
 * <p>
 * The <b>graphic coordinate</b> system is used to draw the graphic, the point of origin is the upper
 * left corner, values increasing to the right and downward respectively.</p>
 * <p>
 * The <b>user coordinate</b> system is defined by the main x-axis and y-axis of the plot using
 * the original units of the plotted values, if possible.<p>
 * 
 */
public interface ERGraphic extends RGraphic {
	
	/**
	 * @see ERGraphic#addListener(Listener)
	 * @see ERGraphic#removeListener(Listener)
	 * @since 1.0
	 */
	interface ListenerLocatorExtension extends Listener {
		
		/**
		 * Is called when a locator is started or restarted (next point).
		 * <p>
		 * The method is called in the display thread.</p>
		 */
		void locatorStarted();
		
		/**
		 * Is called when a locator is stopped.
		 * <p>
		 * The method is called in the display thread.</p>
		 */
		void locatorStopped();
		
	}
	
	
	/**
	 * The default locator stop type indicating that the user does not want to select more points
	 * (OK, in R also called stop, default action for right mouse click).
	 * 
	 * @since 1.0
	 */
	String LOCATOR_DONE= "done"; //$NON-NLS-1$
	
	/**
	 * Locator stop type indicating that the user wants to cancel the locator.
	 * 
	 * @since 1.0
	 */
	String LOCATOR_CANCEL= "cancel"; //$NON-NLS-1$
	
	/**
	 * @since 1.0
	 */
	interface ListenerInstructionsExtension extends Listener {
		
		void instructionsChanged(final boolean reset, final List<ERGraphicInstruction> added);
		
	}
	
	
	@Override
	int getDevId();
	
	/**
	 * Returns the current label for this graphic
	 * <p>
	 * The label can change if the graphic (device) is activated or a locator is started or stopped.
	 * </p>
	 * 
	 * @return the current label
	 */
	String getLabel();
	
	@Override
	boolean isActive();
	
	@Nullable RTool getRHandle();
	
	@Override
	List<? extends ERGraphicInstruction> getInstructions();
	
	Status resize(final double w, final double h);
	
	Status close();
	
	/**
	 * Returns the current message for the graphic.
	 * <p>
	 * The standard OK message means there is no message.  If the message changes messages listeners
	 * registered for this graphic ({@link #addMessageListener(StatusChangeListener)},
	 * {@link #removeMessageListener(StatusChangeListener)}) are notified.</p>
	 * <p>
	 * The message I can be shown for example in the status line of the application.</p>
	 * 
	 * @return the current message to show
	 * @since 1.0
	 */
	Status getMessage();
	
	/**
	 * Registers a new message listener for this graphic.
	 * 
	 * @param listener the listener to add
	 * @since 1.0
	 */
	void addMessageListener(final StatusChangeListener listener);
	
	/**
	 * Removes a message listener registered for this graphic.
	 * 
	 * @param listener the listener to remove
	 * @since 1.0
	 */
	void removeMessageListener(final StatusChangeListener listener);
	
	
	/**
	 * Starts a local locator requesting the user to select a point in the graphic.
	 * <p>
	 * The method starts the locator and returns directly; it does not wait for an answer.
	 * If a locator is already installed for this graphic (local or from R), the method does
	 * nothing and returns an error status.</p>
	 * <p>
	 * The locator callback configures the request and receives the answer(s).  The answer can be
	 * a coordinate located by the user ({@link LocatorCallback#located(double, double)}) or
	 * a stop command ({@link LocatorCallback#stopped(String)}.</p>
	 * <p>
	 * Graphic listeners implementing {@link ListenerLocatorExtension} and registered for this
	 * graphic ({@link #addListener(Listener)}, {@link #removeListener(Listener)}) are notified
	 * if the locator is started and if it is finally stopped.</p>
	 * 
	 * @param callback the callback called to handover the answer(s)
	 * @return An OK status if the locator is started for the given callback, otherwise
	 *     an error status
	 * @see LocatorCallback
	 * @since 1.0
	 */
	Status startLocalLocator(final LocatorCallback callback);
	
	/**
	 * Returns true if any locator (local or from R) is started for this graphic.
	 * 
	 * @return <code>true</code> if a locator is started, otherwise <code>false</code>
	 * @since 1.0
	 */
	boolean isLocatorStarted();
	
	/**
	 * Returns a collection of currently supported stop types.
	 * <p>
	 * If no locator is started, the method returns an empty collection.</p>
	 * 
	 * @return a list of supported stop types
	 * @see LocatorCallback#getStopTypes()
	 * @since 1.0
	 */
	Collection<String> getLocatorStopTypes();
	
	/**
	 * Answers a locator request with the specified coordinates.
	 * <p>
	 * If no locator is started, the method does nothing.</p>
	 * 
	 * @param x the x value of the graphic coordinate
	 * @param y the y value of the graphic coordinate
	 * @since 1.0
	 */
	void returnLocator(final double x, final double y);
	
	/**
	 * Answers a locator request with the specified stop command.
	 * <p>
	 * If no locator is started or does not support the specified type, the method does nothing.</p>
	 * 
	 * @param type the stop type or <code>null</code>
	 * @see LocatorCallback#getStopTypes()
	 * @since 1.0
	 */
	void stopLocator(final String type);
	
	
	/**
	 * Copies the graphic to another R graphic device.
	 * <p>
	 * This is an R based function.  The caller must have exclusive access to the R service.  The
	 * graphic must be available in R.</p>
	 * 
	 * @param toDev the name of the target device
	 * @param toDevFile the name of the file for file based devices
	 * @param toDevArgs other R arguments for the target arguments
	 * @param m
	 * @throws StatusException
	 * @since 1.0
	 */
	void copy(final String toDev, final String toDevFile, final String toDevArgs,
			final ProgressMonitor m) throws StatusException;
	
	/**
	 * Converts a coordinate (x, y) from graphic to user coordinate system.
	 * <p>
	 * This is an R based function.  The caller must have exclusive access to the R service.  The
	 * graphic must be available in R.</p>
	 * 
	 * @param xy the graphic coordinate to convert
	 * @param m
	 * @return the converted user coordinate
	 * @throws StatusException
	 * @see ERGraphic coordinate systems
	 * @since 1.0
	 */
	double[] convertGraphic2User(final double[] xy,
			final ProgressMonitor m) throws StatusException;
	
	/**
	 * Converts a coordinate (x, y) from user to graphic coordinate system.
	 * <p>
	 * This is an R based function.  The caller must have exclusive access to the R service.  The
	 * graphic must be available in R.</p>
	 * 
	 * @param xy the graphic coordinate to convert
	 * @param m
	 * @return the converted user coordinate
	 * @throws StatusException
	 * @see ERGraphic coordinate systems
	 * @since 1.0
	 */
	double[] convertUser2Graphic(final double[] xy,
			final ProgressMonitor m) throws StatusException;
	
}
