/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Manager for a collection of R graphics.
 */
@NonNullByDefault
public interface ERGraphicsManager {
	
	
	interface Listener {
		
		void graphicAdded(ERGraphic graphic);
		
		void graphicRemoved(ERGraphic graphic);
		
	}
	
	interface ListenerShowExtension extends Listener {
		
		/**
		 * If the listener can show the graphic
		 * 
		 * <code>-1</code> not supported
		 * <code>&gt;= 0</code> priority (higher more appropriate)
		 * @param graphic the graphic to show
		 * @return
		 */
		int canShowGraphic(ERGraphic graphic);
		
		/**
		 * If the listener was selected (highest priority) to show the graphic
		 * 
		 * @param graphic the graphic to show
		 */
		void showGraphic(ERGraphic graphic);
		
	}
	
	
	void addListener(Listener listener);
	
	void removeListener(Listener listener);
	
	List<? extends ERGraphic> getAllGraphics();
	
}
