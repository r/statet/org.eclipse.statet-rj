/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics.comclient;

import org.eclipse.statet.rj.server.client.AbstractRJComClient;
import org.eclipse.statet.rj.server.client.AbstractRJComClientGraphicActions;
import org.eclipse.statet.rj.ts.core.RTool;


public class ERClientGraphicActionsFactory implements AbstractRJComClientGraphicActions.Factory {
	
	
	@Override
	public AbstractRJComClientGraphicActions create(final AbstractRJComClient rjs, final Object rHandle) {
		if (rHandle instanceof RTool) {
			return new ERClientGraphicActions(rjs, (RTool) rHandle);
		}
		return null;
	}
	
}
