# Notable Changes in StatET RJ


## RJ 4.5.0 ~ StatET 4.5.0

### Services

  * The data access for tool command handler is enhanced. Implementations of handlers need to be
    adapted.
    
    `ToolCommandHandler` uses `ToolCommandData` instead of a map for the data exchange providing
    access to the raw data (RJObject for RJ) and more flexible, dynamic data conversion to Java
    types. `ToolCommandHandlerUtils` is removed since the functionality is integrated in the new
    tool command data class.
    
    (Bug 574779)

### RServi Node and Pool

  * The configuration and pool API is migrated to make use of `java.time`. Timestamps are
    represented by `Instant` and durations like timeouts by `Duration`, except in MXBeans.
    In the config classes also some related properties are renamed to more meaningful names.
    For property accessors, which still use long for timestamps and durations, the postfix `Millis`
    is added to their names.
    Applications need to be adapted. (Bug 576774)


## RJ 4.4.0 ~ StatET 4.4.0

### RServi Pool

  * The enum of pool node states (`PoolNodeState`) has the additional state DISPOSING for nodes
    marked for disposal but not yet DISPOSED. (Bug 572191)


## RJ 4.3.0 ~ StatET 4.3.0

### General

  * All bundles and distributions of StatET require a Java runtime environment version 11 or
    greater.
    
    All bundles provide an automatic module name identical to their bundle id for use with the Java
    Platform Module System (JPMS).

### Data

  * The data store interfaces RLogicalStore, RIntegerStore and RRawStore provide constants for
    special values like minimum and maximum. (Bug 568187, Bug 568190)

### RServi Pool

  * The pool detects "lost" clients (e.g. clients which crashed or lost the network connection to
    the pool server) again. (Bug 567221)


## RJ 4.2.0 ~ StatET 4.2.0

### Setup

  * The `AppEnvironment` is enhanced and provides bundle resolution now. `RJContext` uses the
    AppEnvironment for bundle resolution by default. Applications need to be adapted.
    
    The bundle `org.eclipse.statet.jcommons.util` provides tools for bundle resolution in different
    types of application.
    Automatic setup of the application environment is supported for
      - Applications based on Eclipse Platform: fully automated
      - Common "simple" Java applications: use CommonsRuntime.check with `autoInit= true`
    
    (Bug 561121)
    
  * The package `org.eclipse.statet.jcommons.status.util` provides implementation of `StatusLogger`
    for the Java-Util Logging and Apache Commons Logging framework. (Bug 561123)


## RJ 4.1.0 ~ StatET 4.1.0

### Setup

  * The `RJContext` for default startup of Java based RJ servers changes. Applications which use
    `RJContext.searchRJLibs` directly or extends the class need to be adapted. (Bug 559616)


## RJ 4.0.0 ~ StatET 4.0.0

### General

  * All bundles require the bundle `org.eclipse.statet.jcommons.util`.

  * All bundles, except bundles of eclient, no longer depends on Eclipse Platform. This includes
    that bundles of ecommons or an 'eruntime' bundle are no longer required.
    
    This causes various changes in the method signatures of the API. New relevant packages are:
      - `org.eclipse.statet.jcommons.status`
      - `org.eclipse.statet.jcommons.runtime`
      - `org.eclipse.statet.jcommons.rmi`
    
    (Bug 541656)

  * Large parts of the API provide nullable annotations. (Bug 539045)

### Services

  * `ToolService` and `RToolService` no longer depends on Eclipse Platform and moved to
    `org.eclipse.statet.jcommons.ts` respectively `org.eclipse.statet.rj.ts.core`.

  * The new method `RService.findData` finds the first object with a specified name in environments
    and returns a fully qualified object with the object itself and the environment where the object
    is found. (Bug 540153)

  * The packages `org.eclipse.statet.env.*` provide common classes for R environment and R pkg
    management as known from the StatET IDE.

### Setup

  * The newly introduced system property `org.eclipse.statet.rj.server.TcpPort` allows to configure
    the TCP ports used by RJ servers. The range must be specified by a value `<min>-<max>`, e.g.
    `org.eclipse.statet.rj.server.TcpPort=40100-40200`. (Bug 540256)

  * The `RJContext` for default startup of Java based RJ servers is more flexible now:
      - It works with `PathEntry` instead of string path providing better support of local path as
        well as URLs, including URIs of nested JAR files.
      - It detects more common bundle filename patterns.
    
    (Bug 541791)
