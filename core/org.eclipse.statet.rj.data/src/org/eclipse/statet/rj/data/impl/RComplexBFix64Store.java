/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RStore;


/**
 * {@link RComplexStore} supporting long length.
 */
@NonNullByDefault
public class RComplexBFix64Store extends AbstractComplexStore
		implements ExternalizableRStore {
	
	
	public static final int SEGMENT_LENGTH= DEFAULT_LONG_DATA_SEGMENT_LENGTH;
	
	
	private final long length;
	
	protected final double [] @NonNull[] realValues;
	protected final double [] @NonNull[] imaginaryValues;
	
	
	public RComplexBFix64Store(final long length) {
		this.length= length;
		this.realValues= new2dDoubleArray(length, SEGMENT_LENGTH);
		this.imaginaryValues= new2dDoubleArray(length, SEGMENT_LENGTH);
	}
	
	public RComplexBFix64Store(final double [] @NonNull[] realValues, final double [] @NonNull[] imaginaryValues) {
		this.length= check2dArrayLength(realValues, SEGMENT_LENGTH);
		if (this.length != check2dArrayLength(imaginaryValues, SEGMENT_LENGTH)) {
			throw new IllegalArgumentException();
		}
		this.realValues= realValues;
		this.imaginaryValues= imaginaryValues;
	}
	
	public RComplexBFix64Store(final double [] @NonNull[] realValues, final double [] @NonNull[] imaginaryValues,
			final boolean @Nullable[] @NonNull[] isNAs) {
		this.length= check2dArrayLength(realValues, SEGMENT_LENGTH);
		if (this.length != check2dArrayLength(imaginaryValues, SEGMENT_LENGTH)) {
			throw new IllegalArgumentException();
		}
		this.realValues= realValues;
		this.imaginaryValues= imaginaryValues;
		if (isNAs != null) {
			if (check2dArrayLength(isNAs, SEGMENT_LENGTH) != this.length) {
				throw new IllegalArgumentException();
			}
			for (int i= 0; i < isNAs.length; i++) {
				final boolean[] isNAi= isNAs[i];
				for (int j= 0; j < isNAi.length; j++) {
					if (isNAi[j]) {
						this.realValues[i][j]= NA_numeric_DOUBLE;
						this.imaginaryValues[i][j]= NA_numeric_DOUBLE;
					}
				}
			}

		}
	}
	
	
	public RComplexBFix64Store(final RJIO io, final long length) throws IOException {
		this.length= length;
		this.realValues= new2dDoubleArray(length, SEGMENT_LENGTH);
		this.imaginaryValues= new2dDoubleArray(length, SEGMENT_LENGTH);
		for (int i= 0; i < this.realValues.length; i++) {
			io.readDoubleData(this.realValues[i], this.realValues[i].length);
			io.readDoubleData(this.imaginaryValues[i], this.imaginaryValues[i].length);
		}
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		for (int i= 0; i < this.realValues.length; i++) {
			io.writeDoubleData(this.realValues[i], this.realValues[i].length);
			io.writeDoubleData(this.imaginaryValues[i], this.imaginaryValues[i].length);
		}
	}
	
	
	@Override
	protected final boolean isStructOnly() {
		return false;
	}
	
	
	@Override
	public final long getLength() {
		return this.length;
	}
	
	@Override
	public boolean isNA(final int idx) {
		final double v= this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
		return (Double.isNaN(v)
				&& (int)Double.doubleToRawLongBits(v) == NA_numeric_INT_MATCH);
	}
	
	@Override
	public boolean isNA(final long idx) {
		final double v= this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
		return (Double.isNaN(v)
				&& (int)Double.doubleToRawLongBits(v) == NA_numeric_INT_MATCH);
	}
	
	@Override
	public void setNA(final int idx) {
		this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
				NA_numeric_DOUBLE;
		this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
				NA_numeric_DOUBLE;
	}
	
	@Override
	public void setNA(final long idx) {
		this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
				NA_numeric_DOUBLE;
		this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
				NA_numeric_DOUBLE;
	}
	
	@Override
	public boolean isNaN(final int idx) {
		final double v= this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
		return (Double.isNaN(v)
				&& (int)Double.doubleToRawLongBits(v) != NA_numeric_INT_MATCH);
	}
	
	@Override
	public boolean isNaN(final long idx) {
		final double value= this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
		return (Double.isNaN(value)
				&& (int)Double.doubleToRawLongBits(value) != NA_numeric_INT_MATCH);
	}
	
	@Override
	public boolean isMissing(final int idx) {
		return (Double.isNaN(
				this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] ));
	}
	
	@Override
	public boolean isMissing(final long idx) {
		return (Double.isNaN(
				this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] ));
	}
	
	@Override
	public double getCplxRe(final int idx) {
		return this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
	}
	
	@Override
	public double getCplxRe(final long idx) {
		return this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
	}
	
	@Override
	public double getCplxIm(final int idx) {
		return this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
	}
	
	@Override
	public double getCplxIm(final long idx) {
		return this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
	}
	
	@Override
	public void setCplx(final int idx, final double real, final double imaginary) {
		if (Double.isNaN(real) || Double.isNaN(imaginary)) {
			this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					NaN_numeric_DOUBLE;
			this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					NaN_numeric_DOUBLE;
		}
		else {
			this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					real;
			this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					imaginary;
		}
	}
	
	@Override
	public void setCplx(final long idx, final double real, final double imaginary) {
		if (Double.isNaN(real) || Double.isNaN(imaginary)) {
			this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					NaN_numeric_DOUBLE;
			this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					NaN_numeric_DOUBLE;
		}
		else {
			this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					real;
			this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					imaginary;
		}
	}
	
	@Override
	public void setNum(final int idx, final double real) {
		if (Double.isNaN(real)) {
			this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					NaN_numeric_DOUBLE;
			this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					NaN_numeric_DOUBLE;
		}
		else {
			this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					real;
			this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
					0.0;
		}
	}
	
	@Override
	public void setNum(final long idx, final double real) {
		if (Double.isNaN(real)) {
			this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					NaN_numeric_DOUBLE;
			this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					NaN_numeric_DOUBLE;
		}
		else {
			this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					real;
			this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
					0.0;
		}
	}
	
	
	@Override
	public @Nullable Complex get(final int idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final double v= this.realValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
		return (!Double.isNaN(v)
				|| (int)Double.doubleToRawLongBits(v) != NA_numeric_INT_MATCH) ?
			new Complex(v, this.imaginaryValues[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH]) :
			null;
	}
	
	@Override
	public @Nullable Complex get(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final double v= this.realValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
		return (!Double.isNaN(v)
				|| (int)Double.doubleToRawLongBits(v) != NA_numeric_INT_MATCH) ?
			new Complex(v, this.imaginaryValues[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)]) :
			null;
	}
	
	@Override
	public @Nullable Complex [] toArray() {
		final int l= checkToArrayLength();
		final var array= new @Nullable Complex [l];
		for (int i= 0, destIdx= 0; i < this.realValues.length; i++) {
			final double[] res= this.realValues[i];
			final double[] ims= this.imaginaryValues[i];
			for (int j= 0; j < res.length; j++, destIdx++) {
				final double v= res[j];
				if (!Double.isNaN(v)
						|| (int)Double.doubleToRawLongBits(v) != NA_numeric_INT_MATCH) {
					array[destIdx]= new Complex(v, ims[j]);
				}
			}
		}
		return array;
	}
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
}
