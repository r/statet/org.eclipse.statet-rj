/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;


@NonNullByDefault
public class RUniqueCharacter32Store extends RCharacter32Store {
	
	
	public RUniqueCharacter32Store() {
		super();
	}
	
	public RUniqueCharacter32Store(final @Nullable String [] initialValues) {
		super(initialValues, initialValues.length);
	}
	
	RUniqueCharacter32Store(final RCharacter32Store source, final boolean reuse) {
		super(source, reuse);
	}
	
	public RUniqueCharacter32Store(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	@Override
	public void setChar(final int idx, final String value) {
		if (indexOf(value) >= 0) {
			if (indexOf(value) == idx) {
				return;
			}
			throw new IllegalArgumentException();
		}
		super.setChar(idx, value);
	}
	
	@Override
	public void setChar(final long idx, final String value) {
		if (idx < 0 || idx >= getLength()) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		setChar((int)idx, value);
	}
	
	@Override
	public void insertChar(final int idx, final String value) {
		if (indexOf(value) >= 0) {
			throw new IllegalArgumentException();
		}
		super.insertChar(idx, value);
	}
	
	@Override
	public void setNA(final int idx) {
	}
	
	@Override
	public void setNA(final long idx) {
	}
	
	@Override
	public void insertNA(final int idx) {
	}
	
	@Override
	public void insertNA(final int[] idxs) {
	}
	
	protected void insertAuto(final int idx) {
		insertChar(idx, createAuto(idx));
	}
	
	protected String createAuto(final int idx) {
		final String nr= Integer.toString(idx+1);
		if (indexOf(nr) < 0) {
			return nr;
		}
		for (int i= 1; ; i++) {
			final String sub= nr+'.'+Integer.toString(i);
			if (indexOf(sub) < 0) {
				return sub;
			}
		}
	}
	
}
