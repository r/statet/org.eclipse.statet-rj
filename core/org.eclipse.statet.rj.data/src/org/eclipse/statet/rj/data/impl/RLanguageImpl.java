/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RLanguageImpl extends AbstractRObject
		implements RLanguage, ExternalizableRObject {
	
	
	public static final String getBaseClassname(final byte type) {
		switch (type) {
		case NAME:
			return CLASSNAME_NAME;
		case CALL:
			return CLASSNAME_CALL;
		default:
			return CLASSNAME_EXPRESSION;
		}
	}
	
	
	private byte type;
	
	private String className1;
	
	private @Nullable String source;
	
	
	public RLanguageImpl(final byte type, final String source, final @Nullable String className1) {
		this.type= type;
		this.className1= (className1 != null) ? className1 : getBaseClassname(type);
		this.source= source;
	}
	
	public RLanguageImpl(final byte type, final @Nullable String className1) {
		this.type= type;
		this.className1= (className1 != null) ? className1 : getBaseClassname(type);
	}
	
	@SuppressWarnings("null")
	public RLanguageImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		final int options= io.readInt();
		//-- special attributes
		this.type= io.readByte();
		this.className1= ((options & RObjectFactory.O_CLASS_NAME) != 0) ?
				io.readString() :
				getBaseClassname(this.type);
		//-- data
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0) {
			this.source= io.readString();
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		int options= 0;
		if (!this.className1.equals(getBaseClassname(this.type))) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		io.writeInt(options);
		io.writeByte(this.type);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		//-- data
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0) {
			io.writeString(this.source);
		}
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_LANGUAGE;
	}
	
	@Override
	public byte getLanguageType() {
		return this.type;
	}
	
	@Override
	public String getRClassName() {
		return this.className1;
	}
	
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable String getSource() {
		return this.source;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=RLanguage, class=").append(getRClassName());
		if (this.source != null) {
			sb.append("\n\tsource: ");
			final int idx= this.source.indexOf('\n');
			if (idx >= 0) {
				sb.append(this.source.substring(0, idx));
				sb.append(" ...");
			}
			else {
				sb.append(this.source);
			}
		}
		return sb.toString();
	}
	
}
