/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An R object is of the type {@link RObject#TYPE_LIST list}, if the object is
 * an R list but not an R data frame (see {@link #TYPE_DATAFRAME}).  Such an R
 * list object is represented by an instance of this interface.
 * <p>
 * The R function <code>typeof(object)</code> returns 'list' or 'pairlist' for
 * objects of this type.</p>
 * <p>
 * The interfaces for R objects of the type {@value RObject#TYPE_DATAFRAME} -
 * {@link RDataFrame}, {@link RObject#TYPE_S4OBJECT} - {@link RS4Object} and
 * {@link RObject#TYPE_ENVIRONMENT} - {@link REnvironment} extends this interface for the
 * purpose of a uniform API.
 * Objects of this type does not necessary provide the full functionality and the
 * methods can have special meaning and conditions; see the documentation of these
 * interfaces.</p>
 * <p>
 * Indexes are zero-based (as usual in Java) and not one-base like in R.</p>
 */
@NonNullByDefault
public interface RList extends RObject {
	
	
	/**
	 * Returns the length of the object. The length of a {@link RObject#TYPE_LIST list}
	 * is the count of list items.
	 * 
	 * @return the length
	 */
	@Override
	long getLength();
	
	/**
	 * Returns the names of the list items.
	 * 
	 * @return the item names
	 */
	@Nullable RCharacterStore getNames();
	
	/**
	 * Returns the name of the item at the specified index.
	 * <p>
	 * This is equivalent to <code>getNames().getChar(idx)</code>.</p>
	 * 
	 * @param idx the index (zero-based) of the item
	 * @return the name of the item
	 * @throws IndexOutOfBoundsException if <code>idx</code> &lt; 0 or <code>idx</code> &ge; length
	 */
	@Nullable String getName(int idx);
	
	/**
	 * Returns the name of the item at the specified index.
	 * <p>
	 * This is equivalent to <code>getNames().getChar(idx)</code>.</p>
	 * 
	 * @param idx the index (zero-based) of the item
	 * @return the name of the item
	 * @throws IndexOutOfBoundsException if <code>idx</code> &lt; 0 or <code>idx</code> &ge; length
	 */
	@Nullable String getName(long idx);
	
	/**
	 * Returns the item at the specified index.
	 * 
	 * @param idx the index (zero-based) of the item
	 * @return the item
	 * @throws IndexOutOfBoundsException if <code>idx</code> &lt; 0 or <code>idx</code> &ge; length
	 */
	@Nullable RObject get(int idx);
	
	/**
	 * Returns the item at the specified index.
	 * 
	 * @param idx the index (zero-based) of the item
	 * @return the item
	 * @throws IndexOutOfBoundsException if <code>idx</code> &lt; 0 or <code>idx</code> &ge; length
	 */
	@Nullable RObject get(long idx);
	
	/**
	 * Returns the item with the specified name. If multiple items have that name,
	 * the first item with the given name is picked.
	 * 
	 * @param name the name of the item
	 * @return the item or <code>null</code> (if no item with the specified name exists)
	 */
	@Nullable RObject get(String name);
	
//	/**
//	 * Returns an array with the R object items of the list.
//	 * <p>
//	 * The array is newly created for each call of this method.</p>
//	 * 
//	 * @return an array with the items of the list
//	 */
//	RObject[] toArray();
	
//	void insert(int idx, String name, RObject item);
//	void add(String name, RObject item);
//	boolean set(int idx, RObject item);
//	boolean set(String name, RObject item);
//	void remove(int idx);
	
}
