/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class AbstractComplexStore extends AbstractRStore<RComplexStore.Complex>
		implements RComplexStore {
	
	
	protected static final String toChar(final double real, final double imaginary) {
		final StringBuilder sb= new StringBuilder();
		if (real == Double.POSITIVE_INFINITY) {
			sb.append("Inf");
		}
		else if (real == Double.NEGATIVE_INFINITY) {
			sb.append("-Inf");
		}
		else {
			sb.append(real);
		}
		if (imaginary == Double.POSITIVE_INFINITY) {
			sb.append("+Infi");
		}
		else if (imaginary == Double.NEGATIVE_INFINITY) {
			sb.append("-Infi");
		}
		else if (Double.isNaN(imaginary)) {
			sb.append("+NaNi");
		}
		else if (imaginary >= 0) {
			sb.append('+');
			sb.append(imaginary);
			sb.append('i');
		}
		else {
			sb.append(imaginary);
			sb.append('i');
		}
		return sb.toString();
	}
	
	
	@Override
	public final byte getStoreType() {
		return RStore.COMPLEX;
	}
	
	@Override
	public final String getBaseVectorRClassName() {
		return RObject.CLASSNAME_COMPLEX;
	}
	
	
	@Override
	public final String getChar(final int idx) {
		return toChar(getCplxRe(idx), getCplxIm(idx));
	}
	
	@Override
	public final String getChar(final long idx) {
		return toChar(getCplxRe(idx), getCplxIm(idx));
	}
	
	
	@Override
	public long indexOf(final int integer, final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public abstract @Nullable Complex [] toArray();
	
}
