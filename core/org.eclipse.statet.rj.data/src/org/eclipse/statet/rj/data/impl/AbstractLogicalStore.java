/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RLogicalStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class AbstractLogicalStore extends AbstractRStore<Boolean>
		implements RLogicalStore {
	
	
	protected static final String toChar(final boolean logi) {
		return (logi) ? "TRUE" : "FALSE";
	}
	
	
	@Override
	public final byte getStoreType() {
		return RStore.LOGICAL;
	} 
	
	@Override
	public final String getBaseVectorRClassName() {
		return RObject.CLASSNAME_LOGICAL;
	}
	
	
	@Override
	public final int getInt(final int idx) {
		return getLogi(idx) ? 1 : 0;
	}
	
	@Override
	public final int getInt(final long idx) {
		return getLogi(idx) ? 1 : 0;
	}
	
	@Override
	public final void setInt(final int idx, final int integer) {
		setLogi(idx, integer != 0);
	}
	
	@Override
	public final void setInt(final long idx, final int integer) {
		setLogi(idx, integer != 0);
	}
	
	@Override
	public final String getChar(final int idx) {
		return toChar(getLogi(idx));
	}
	
	@Override
	public final String getChar(final long idx) {
		return toChar(getLogi(idx));
	}
	
	@Override
	public void setChar(final int idx, final String character) {
		setLogi(idx, AbstractCharacterStore.toLogi(character));
	}
	
	@Override
	public void setChar(final long idx, final String character) {
		setLogi(idx, AbstractCharacterStore.toLogi(character));
	}
	
	@Override
	public long indexOf(final String character, final long fromIdx) {
		try {
			return indexOf(AbstractCharacterStore.toLogi(character) ? 1 : 0, fromIdx);
		}
		catch (final NumberFormatException e) {
			return -1;
		}
	}
	
	@Override
	public byte getRaw(final int idx) {
		return getLogi(idx) ? (byte) 1 : (byte) 0;
	}
	
	@Override
	public byte getRaw(final long idx) {
		return getLogi(idx) ? (byte) 1 : (byte) 0;
	}
	
	@Override
	public void setRaw(final int idx, final byte raw) {
		setLogi(idx, raw != 0);
	}
	
	@Override
	public void setRaw(final long idx, final byte raw) {
		setLogi(idx, raw != 0);
	}
	
	
	@Override
	public abstract @Nullable Boolean [] toArray();
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		final long length= getLength();
		if (LOGICAL != other.getStoreType() || length != other.getLength()) {
			return false;
		}
		if (length < 0) {
			return true;
		}
		else if (length <= Integer.MAX_VALUE) {
			final int ilength= (int)length;
			for (int idx= 0; idx < ilength; idx++) {
				if (!(isNA(idx) ? other.isNA(idx) :
						getLogi(idx) == other.getLogi(idx) )) {
					return false;
				}
			}
		}
		else {
			for (long idx= 0; idx < length; idx++) {
				if (!(isNA(idx) ? other.isNA(idx) :
						getLogi(idx) == other.getLogi(idx) )) {
					return false;
				}
			}
		}
		return true;
	}
	
}
