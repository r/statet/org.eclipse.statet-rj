/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.rj.data.RCharacterStore;


public class SimpleRList<T> {
	
	
	private final RCharacter32Store names;
	private final T[] values;
	
	
	public SimpleRList(final T[] values, final RCharacter32Store names) {
		this.names= names;
		this.values= values;
	}
	
	
	public int getLength() {
		return this.names.length();
	}
	
	public RCharacterStore getNames() {
		return this.names;
	}
	
	public String getName(final int idx) {
		return this.names.get(idx);
	}
	
	public T get(final int idx) {
		return this.values[idx];
	}
	
	public T get(final String name) {
		final int idx= this.names.indexOf(name, 0);
		if (idx >= 0) {
			return this.values[idx];
		}
		return null;
	}
	
}
