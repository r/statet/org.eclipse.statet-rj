/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Interface for R data stores of type {@link RStore#FACTOR}.
 * <p>
 * An R data store implements this interface if the R function
 * <code>class(object)</code> returns 'factor'.</p>
 */
@NonNullByDefault
public interface RFactorStore extends RStore<Integer> {
	
	
	int MIN_INT= 0x00000001;
	int MAX_INT= RIntegerStore.MAX_INT;
	
	
	// TODO Docu && Constructors (-> 1-based codes)
	// TODO getFactorName(code) for 1-based code
	
	boolean isOrdered();
	
	RCharacterStore getLevels();
	int getLevelCount();
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * May return {@code null} if it has an extra NA level.
	 */
	@Override
	@Nullable String getChar(final int idx);
	/**
	 * {@inheritDoc}
	 * 
	 * May return {@code null} if it has an extra NA level.
	 */
	@Override
	@Nullable String getChar(final long idx);
	RCharacterStore toCharacterData();
	
	
	@Override
	@Nullable Integer get(int idx);
	@Override
	@Nullable Integer get(long idx);
	
	@Override
	@Nullable Integer [] toArray();
	
//	void addLevel(final String label);
//	void renameLevel(final String oldLabel, final String newLabel);
//	void insertLevel(final int position, final String label);
//	void removeLevel(final String label);
	
	
}
