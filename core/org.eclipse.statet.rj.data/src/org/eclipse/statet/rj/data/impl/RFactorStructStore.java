/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RStore;


public class RFactorStructStore extends AbstractFactorStore {
	
	
	public static RFactorStructStore addLevels(final RFactorStore store, final RCharacterStore levels) {
		final long levelCount= levels.getLength();
		if (levelCount > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("levelCount > 2^31-1");
		}
		return new RFactorStructStore(store.isOrdered(), (int)levels.getLength()) {
			@Override
			public RCharacterStore getLevels() {
				return levels;
			}
		};
	}
	
	
	private final int levelCount;
	
	
	public RFactorStructStore(final boolean isOrdered, final int levelCount) {
		this.isOrdered= isOrdered;
		this.levelCount= levelCount;
	}
	
	
	@Override
	protected final boolean isStructOnly() {
		return true;
	}
	
	
	@Override
	public final long getLength() {
		return -1;
	}
	
	@Override
	public boolean isNA(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isNA(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isMissing(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isMissing(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public final int getLevelCount() {
		return this.levelCount;
	}
	
	@Override
	public RCharacterStore getLevels() {
		throw new UnsupportedOperationException();
	}
	
	public void insertLevel(final int position, final String label) {
		throw new UnsupportedOperationException();
	}
	
	public void removeLevel(final String label) {
		throw new UnsupportedOperationException();
	}
	
	public void renameLevel(final String oldLabel, final String newLabel) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public RCharacterStore toCharacterData() {
		return new RCharacterStructStore();
	}
	
	@Override
	public Integer get(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Integer get(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public final Integer[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public long indexOfNA(final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long indexOf(final int integer, final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long indexOf(final String character, final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		return (FACTOR == other.getStoreType()
				&& this.isOrdered == ((RFactorStore) other).isOrdered()
				&& this.levelCount == ((RFactorStore) other).getLevelCount()
				&& -1 == other.getLength() );
	}
	
}
