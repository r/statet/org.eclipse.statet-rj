/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Interface for R data stores of type {@link RStore#INTEGER}.
 * <p>
 * An R data store implements this interface if the R function
 * <code>typeof(object)</code> returns 'integer'.</p>
 */
@NonNullByDefault
public interface RIntegerStore extends RStore<Integer> {
	
	
	int MIN_INT= 0x80000001; // = Integer.MIN_VALUE + 1
	int MAX_INT= 0x7fffffff; // = Integer.MAX_VALUE
	
	
	@Override
	String getChar(final int idx);
	@Override
	String getChar(final long idx);
	
	
	@Override
	@Nullable Integer get(int idx);
	@Override
	@Nullable Integer get(long idx);
	
	@Override
	@Nullable Integer [] toArray();
	
}
