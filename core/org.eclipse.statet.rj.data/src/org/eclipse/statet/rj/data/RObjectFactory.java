/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RObjectFactory {
	
	/** Flag to fetch only the structure but not the data (store) of the objects */
	int F_ONLY_STRUCT=                  1 << 0;
	
	/** XXX: Not yet implemented */
	int F_WITH_ATTR=                    1 << 1;
	
	/** Flag to fetch additional dbg detail */
	int F_WITH_DBG=                     1 << 2;
	
	/** Flag to load environments directly instead of the reference only */
	int F_LOAD_ENVIRONMENT=             1 << 4;
	
	/** Flag to eval all promises directly */
	int F_LOAD_PROMISE=                 1 << 5;
	
	
	int O_LENGTHGRADE_MASK= 7; // 3 bits
	
	int O_WITH_ATTR= 1 << 3;
	
	int O_CLASS_NAME= 1 << 4;
	
	int O_NO_CHILDREN= 1 << 5;
	
	int O_WITH_NAMES= 1 << 6;
	
	
//	RArgument createArgument(String name, String defaultSource);
//	RFunction createFunction(RArgument[] argument);
//	
	<TData extends RStore<?>> RVector<TData> createVector(TData data);
	<TData extends RStore<?>> RArray<TData> createArray(TData data, int[] dim);
	<TData extends RStore<?>> RArray<TData> createMatrix(TData data, int dim1, int dim2);
	RList createList(RObject [] components, @Nullable String @Nullable[] names);
//	RDataFrame createDataFrame(RData[] columns, String[] columnNames, String[] rowNames);
	
	RLanguage createName(String name);
	RLanguage createExpression(String expr);
	
	RLogicalStore createLogiData(boolean [] logiValues);
	RIntegerStore createIntData(int [] intValues);
	RNumericStore createNumData(double [] numValues);
	RComplexStore createCplxData(double [] reValues, double [] imValues);
	RCharacterStore createCharData(@Nullable String [] charValues);
	RRawStore createRawData(byte [] values);
	RFactorStore createFactorData(int[] codes, String [] levels);
	
	void writeObject(RObject object, RJIO io) throws IOException;
	RObject readObject(RJIO io) throws IOException;
	
	void writeStore(RStore<?> data, RJIO io) throws IOException;
	RStore<?> readStore(RJIO io, long length) throws IOException;
	
	void writeAttributeList(RList list, RJIO io) throws IOException;
	RList readAttributeList(RJIO io) throws IOException;
	
	void writeNames(@Nullable RStore<?> names, RJIO io) throws IOException;
	@Nullable RStore<?> readNames(RJIO io, long length) throws IOException;
	
}
