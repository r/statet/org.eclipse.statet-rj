/*=============================================================================#
 # Copyright (c) 2019, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.SystemUtils;


@NonNullByDefault
public class BasicREnvConfigurationTest {
	
	
	private static class TestREnvConfiguration extends BasicREnvConfiguration {
		
		
		private final byte expliciteOs;
		
		
		public TestREnvConfiguration(final REnv rEnv, final Path stateRootDirectoryPath,
				final byte expliciteOs) {
			super(rEnv, stateRootDirectoryPath);
			this.expliciteOs= expliciteOs;
		}
		
		
		@Override
		protected byte getOs() {
			return this.expliciteOs;
		}
		
	}
	
	
	private final Path statePath;
	
	
	public BasicREnvConfigurationTest() throws IOException {
		this.statePath= Files.createTempDirectory("rj.renv.core-tests");
	}
	
	
	private TestREnvConfiguration createCommonLocalConfig(final byte os) {
		try {
			final String id= "test";
			final Path path= this.statePath.resolve(id);
			Files.createDirectories(path);
			final TestREnvConfiguration config= new TestREnvConfiguration(
					new BasicREnvManager.ActualREnv(id), path, os );
			config.setFlags(TestREnvConfiguration.LOCAL | TestREnvConfiguration.SPEC_SETUP);
			return config;
		}
		catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Test
	public void setget_RHomeDirectory_Win() throws IOException {
		final TestREnvConfiguration config= createCommonLocalConfig(SystemUtils.OS_WIN);
		
		config.setRHomeDirectory("C:/R-3.5");
		assertEquals("C:/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("C:/R-3.5/");
		assertEquals("C:/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("C:\\R-3.5");
		assertEquals("C:/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("C:\\R-3.5\\");
		assertEquals("C:/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
	}
	
	@Test
	public void setget_RHomeDirectory_Nix() throws IOException {
		final TestREnvConfiguration config= createCommonLocalConfig((byte) 0);
		
		config.setRHomeDirectory("/opt/R-3.5");
		assertEquals("/opt/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("/opt/R-3.5/");
		assertEquals("/opt/R-3.5", config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("/opt/R-3.5/");
		config.setRHomeDirectory(null);
		assertNull(config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
		
		config.setRHomeDirectory("/opt/R-3.5/");
		config.setRHomeDirectory("");
		assertNull(config.getRHomeDirectory());
		assertNull(config.getRHomeDirectoryPath());
	}
	
	@Test
	public void setget_RArch_Win() {
		final TestREnvConfiguration config= createCommonLocalConfig(SystemUtils.OS_WIN);
		
		config.setRArch("x64");
		assertEquals(SystemUtils.ARCH_X86_64, config.getRArch());
		
		config.setRArch("/x64");
		assertEquals(SystemUtils.ARCH_X86_64, config.getRArch());
		
		config.setRArch("amd64");
		assertEquals(SystemUtils.ARCH_X86_64, config.getRArch());
		
		config.setRArch("i386");
		assertEquals(SystemUtils.ARCH_X86_32, config.getRArch());
		
		config.setRArch("/i386");
		assertEquals(SystemUtils.ARCH_X86_32, config.getRArch());
		
		config.setRArch("x64");
		config.setRArch(null);
		assertNull(config.getRArch());
		
		config.setRArch("x64");
		config.setRArch("");
		assertNull(config.getRArch());
	}
	
	@Test
	public void setget_RDocDirectory_Win() {
		final TestREnvConfiguration config= createCommonLocalConfig(SystemUtils.OS_WIN);
		
		config.setRDocDirectory("C:/R-3.5/doc");
		assertEquals("C:/R-3.5/doc", config.getRDocDirectory());
		assertNull(config.getRDocDirectoryPath());
		
		config.setRDocDirectory("C:/R-3.5/doc/");
		assertEquals("C:/R-3.5/doc", config.getRDocDirectory());
		assertNull(config.getRDocDirectoryPath());
		
		config.setRDocDirectory("${r_home}/doc");
		assertEquals("${r_home}/doc", config.getRDocDirectory());
		assertNull(config.getRDocDirectoryPath());
		
		config.setRDocDirectory("C:/R-3.5/doc");
		config.setRDocDirectory(null);
		assertNull(config.getRDocDirectory());
		assertNull(config.getRDocDirectoryPath());
		
		config.setRDocDirectory("C:/R-3.5/doc");
		config.setRDocDirectory("");
		assertNull(config.getRDocDirectory());
		assertNull(config.getRDocDirectoryPath());
	}
	
	@Test
	public void setget_RShareDirectory_Win() {
		final TestREnvConfiguration config= createCommonLocalConfig(SystemUtils.OS_WIN);
		
		config.setRShareDirectory("C:/R-3.5/share");
		assertEquals("C:/R-3.5/share", config.getRShareDirectory());
		assertNull(config.getRShareDirectoryPath());
		
		config.setRShareDirectory("C:/R-3.5/share/");
		assertEquals("C:/R-3.5/share", config.getRShareDirectory());
		assertNull(config.getRShareDirectoryPath());
		
		config.setRShareDirectory("${r_home}/share");
		assertEquals("${r_home}/share", config.getRShareDirectory());
		assertNull(config.getRShareDirectoryPath());
		
		config.setRShareDirectory("C:/R-3.5/share");
		config.setRShareDirectory(null);
		assertNull(config.getRShareDirectory());
		assertNull(config.getRShareDirectoryPath());
		
		config.setRShareDirectory("C:/R-3.5/share");
		config.setRShareDirectory("");
		assertNull(config.getRShareDirectory());
		assertNull(config.getRShareDirectoryPath());
	}
	
	@Test
	public void setget_RIncludeDirectory_Win() {
		final TestREnvConfiguration config= createCommonLocalConfig(SystemUtils.OS_WIN);
		
		config.setRIncludeDirectory("C:/R-3.5/include");
		assertEquals("C:/R-3.5/include", config.getRIncludeDirectory());
		assertNull(config.getRIncludeDirectoryPath());
		
		config.setRIncludeDirectory("C:/R-3.5/include/");
		assertEquals("C:/R-3.5/include", config.getRIncludeDirectory());
		assertNull(config.getRIncludeDirectoryPath());
		
		config.setRIncludeDirectory("${r_home}/include");
		assertEquals("${r_home}/include", config.getRIncludeDirectory());
		assertNull(config.getRIncludeDirectoryPath());
		
		config.setRIncludeDirectory("C:/R-3.5/include");
		config.setRIncludeDirectory(null);
		assertNull(config.getRIncludeDirectory());
		assertNull(config.getRIncludeDirectoryPath());
		
		config.setRIncludeDirectory("C:/R-3.5/include");
		config.setRIncludeDirectory("");
		assertNull(config.getRIncludeDirectory());
		assertNull(config.getRIncludeDirectoryPath());
	}
	
}
