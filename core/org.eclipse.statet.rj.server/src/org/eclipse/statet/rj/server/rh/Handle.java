/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.rh;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Handle {
	
	
	private static final char[] DIGITS= new char[] {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
	};
	
	static String toString(long p) {
		final char[] c= new char[18];
		c[0]= '0';
		c[1]= 'x';
		for (int i= 0; i < 16; i++) {
			c[17 - i]= DIGITS[(int) p & 0xF];
			p >>>= 4;
		}
		return new String(c, 0, 18);
	}
	
	
	public final long p;
	
	
	public Handle(final long p) {
		this.p= p;
	}
	
	
	@Override
	public int hashCode() {
		return (int) (this.p ^ (this.p >>> 32));
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof Handle && this.p == ((Handle) obj).p));
	}
	
	
	@Override
	public String toString() {
		return toString(this.p);
	}
	
}
