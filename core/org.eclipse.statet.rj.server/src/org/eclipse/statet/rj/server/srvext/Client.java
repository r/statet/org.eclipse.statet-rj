/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srvext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class Client {
	
	
	private final String username;
	final String clientId;
	
	public final byte slot;
	
	
	public Client(final String username, final String clientId, final byte slot) {
		this.username= username;
		this.clientId= clientId;
		this.slot= slot;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	
	@Override
	public String toString() {
		return String.format("Client %1$08X (slot= %4$s) '%2$s'",
				hashCode(), this.username, this.clientId, this.slot );
	}
	
}
