/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


public class DbgFilterState implements RJIOExternalizable {
	
	
	private final int stepFilterState;
	
	
	public DbgFilterState(final boolean stepFilterEnabled) {
		this.stepFilterState= (stepFilterEnabled) ? 0x1 : 0x0;
	}
	
	public DbgFilterState(final RJIO io) throws IOException {
		this.stepFilterState= io.readInt();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.stepFilterState);
	}
	
	
	public boolean stepFilterEnabled() {
		return ((this.stepFilterState & 0x1) != 0);
	}
	
}
