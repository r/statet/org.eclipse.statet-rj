/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srv.engine;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.server.REngine;
import org.eclipse.statet.rj.server.RjsComObject;
import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srvext.Client;


@NonNullByDefault
public final class REngineImpl implements REngine {
	
	
	private final Server publicServer;
	private final SrvEngine srvEngine;
	private final Client client;
	
	
	public REngineImpl(final Server publicServer, final SrvEngine srvEngine, final Client client) {
		this.publicServer= nonNullAssert(publicServer);
		if (Boolean.getBoolean("org.eclipse.statet.rj.server.srv.engine.ComLogger.enable")) {
			final Logger logger= Logger.getLogger("org.eclipse.statet.rj.server.srv");
			this.srvEngine= new ComLoggingSrvEngine(srvEngine, logger);
		}
		else {
			this.srvEngine= nonNullAssert(srvEngine);
		}
		this.client= client;
	}
	
	
	@Override
	public Server getPublic() throws RemoteException {
		return this.publicServer;
	}
	
	@Override
	public Map<String, Object> getPlatformData() {
		return this.srvEngine.getPlatformData();
	}
	
	@Override
	public void setProperties(final Map<String, ? extends @NonNull Object> properties)
			throws RemoteException {
		this.srvEngine.setProperties(this.client, properties);
	}
	
	@Override
	public void disconnect()
			throws RemoteException {
		this.srvEngine.disconnect(this.client);
	}
	
	@Override
	public RjsComObject runAsync(final RjsComObject com)
			throws RemoteException {
		return this.srvEngine.runAsync(this.client, com);
	}
	
	@Override
	public RjsComObject runMainLoop(final RjsComObject com)
			throws RemoteException {
		return this.srvEngine.runMainLoop(this.client, com);
	}
	
	@Override
	public boolean isClosed()
			throws RemoteException {
		return (this.srvEngine.getCurrentClient() != this.client);
	}
	
}
