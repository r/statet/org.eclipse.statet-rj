/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.rh;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.RjsException;


@NonNullByDefault
public interface RhEngine {
	
	
	/**
	 * Function call argument.
	 */
	final class Arg {
		
		
		private final @Nullable Handle name;
		private Handle value;
		
		
		public Arg(final @Nullable Handle name, final Handle value) {
			this.name= name;
			this.value= value;
		}
		
		
		public @Nullable Handle getName() {
			return this.name;
		}
		
		public Handle getValue() {
			return this.value;
		}
		
		public void setValue(final Handle value) {
			this.value= value;
		}
		
	}
	
	
	Handle preserve(final Handle obj);
	void releasePreserved(final Handle obj);
	
	RhWeakRef newWeakRef(final Handle obj, final RhRefListener listener);
	
	
	Handle getNull();
	
	Handle getBaseEnv();
	Handle getGlobalEnv();
	Handle getEmptyEnv();
	
	/**
	 * Creates a new environment object.
	 * @param parentEnv The handle of the parent environment (environment)
	 * @return The handle of the new object (environment)
	 */
	Handle newEnv(final Handle parentEnv) throws RjsException;
	
	/**
	 * Returns the parent environment of the specified environment.
	 * @param env The handle of the environment (environment)
	 * @return The handle (environment) or <code>null</code>, if it has no parent environment
	 * @throws RjsException
	 */
	@Nullable Handle getParentEnv(final Handle env) throws RjsException;
	
	
	/**
	 * Creates a name object.
	 * @param name The name
	 * @return The handle (name)
	 * @throws RjsException
	 */
	Handle asName(final String name) throws RjsException;
	
	/**
	 * Creates a new call object.
	 * @param fun The function to call
	 * @param funArgs The function arguments
	 * @return The handle of the new object (call)
	 * @throws RjsException
	 */
	Handle newCall(final Handle fun, final Arg... funArgs) throws RjsException;
	
	
	/**
	 * Evaluates the specified expression.
	 * @param expr The handle of the expression to evaluate
	 * @param env The handle of the environment (environment), where to evaluate the expression
	 * @return The handle of the result
	 * @throws RjsException
	 */
	Handle eval(final Handle expr, final Handle env) throws RjsException;
	
	
	/**
	 * Returns the attribute value of the specified object.
	 * @param obj The handle of the object
	 * @param name The handle of the attribute name (name)
	 * @return The handle of the attribute value
	 * @throws RjsException
	 */
	@Nullable Handle getAttribute(final Handle obj, final Handle name) throws RjsException;
	
	
	/**
	 * Returns the search path environments.
	 * @return A list with handles (environment)
	 * @throws RjsException
	 */
	List<Handle> getSearchPath() throws RjsException;
	
	List<Handle> getStackFrames() throws RjsException;
	
}
