/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;


/**
 * Abstract runnable for R tool.
 * 
 * Sub class should implement at least the method {@link #run(RToolService, ProgressMonitor)},
 * which is called by the R tool and gives access to the {@link RToolService} API.
 * 
 * @since 1.2
 */
@NonNullByDefault
public abstract class AbstractRToolRunnable implements ToolRunnable {
	
	
	private final String typeId;
	private final String label;
	
	
	public AbstractRToolRunnable(final String typeId, final String label) {
		this.typeId= typeId;
		this.label= label;
	}
	
	
	@Override
	public String getTypeId() {
		return this.typeId;
	}
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return tool.isProvidingFeatureSet(RTool.R_SERVICE_FEATURE_ID);
	}
	
	@Override
	public boolean changed(final int event, final Tool tool) {
		return true;
	}
	
	@Override
	public void run(final ToolService service,
			final ProgressMonitor m) throws StatusException {
		run((RToolService) service, m);
	}
	
	protected abstract void run(final RToolService r,
			final ProgressMonitor m) throws StatusException;
	
}
