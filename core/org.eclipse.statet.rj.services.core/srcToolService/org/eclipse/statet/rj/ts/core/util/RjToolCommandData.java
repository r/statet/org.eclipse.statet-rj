/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core.util;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.BasicToolCommandData;

import org.eclipse.statet.rj.data.RDataJConverter;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;


@NonNullByDefault
public class RjToolCommandData extends BasicToolCommandData {
	
	
	private final RList rjData;
	
	private final RDataJConverter rjConverter;
	
	
	public RjToolCommandData(final RList rjData) {
		this.rjData= rjData;
		this.rjConverter= new RDataJConverter();
		this.rjConverter.setKeepArray1(true);
	}
	
	
	@Override
	public @Nullable Object getRawData(final String key) {
		return this.rjData.get(key);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <TValue> @Nullable TValue convert(@Nullable Object data, final Class<TValue> type) {
		if (data instanceof RObject) {
			if (RObject.class.isAssignableFrom(type)) {
				return null;
			}
			data= this.rjConverter.toJava((RObject)data);
			if (data == null || type.isInstance(data)) {
				return (TValue)data;
			}
		}
		
		return super.convert(data, type);
	}
	
	
	protected RList getRJReturnData(final RObjectFactory rObjectFactory) {
		final var returnData= getReturnData();
		this.rjConverter.setRObjectFactory(rObjectFactory);
		return (RList)this.rjConverter.toRJ(returnData);
	}
	
}
