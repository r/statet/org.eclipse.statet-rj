/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.net.URI;
import java.nio.file.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;


@NonNullByDefault
public interface REnvConfiguration extends RLibPaths {
	
	
	String R_HOME_DIRECTORY_VAR_NAME= "r_home"; //$NON-NLS-1$
	String R_HOME_DIRECTORY_VAR_STRING= "${r_home}"; //$NON-NLS-1$
	
	String SHARED_DIRECTORY= "directory"; //$NON-NLS-1$
	String SHARED_SERVER= "server"; //$NON-NLS-1$
	
	
	REnv getREnv();
	
	String getName();
	
	boolean isLocal();
	boolean isRemote();
	
	Status getValidationStatus();
	
	
	@Nullable String getRHomeDirectory();
	@Nullable Path getRHomeDirectoryPath();
	
	@Nullable String getRArch();
	
	@Nullable String getRShareDirectory();
	@Nullable Path getRShareDirectoryPath();
	
	@Nullable String getRIncludeDirectory();
	@Nullable Path getRIncludeDirectoryPath();
	
	@Nullable String getRDocDirectory();
	@Nullable Path getRDocDirectoryPath();
	
	String getStateSharedType();
	@Nullable String getStateSharedDirectory();
	@Nullable Path getStateSharedDirectoryPath();
	@Nullable String getStateSharedServer();
	@Nullable URI getStateSharedServerUri();
	
	Path getStateLocalDirectoryPath();
	
}
