/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BasicRPkgBuilt extends BasicRPkg implements RPkgBuilt {
	
	
	private final String title;
	
	private final String built;
	
	private final RLibLocation libLocation;
	
	
	public BasicRPkgBuilt(final String name, final RNumVersion version, final String built,
			final String title, final RLibLocation libLocation) {
		super(name, version);
		
		this.title= nonNullAssert(title);
		this.built= nonNullAssert(built);
		this.libLocation= nonNullAssert(libLocation);
	}
	
	
	@Override
	public String getTitle() {
		return this.title;
	}
	
	@Override
	public String getBuilt() {
		return this.built;
	}
	
	@Override
	public RLibLocation getLibLocation() {
		return this.libLocation;
	}
	
}
