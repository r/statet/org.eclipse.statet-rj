/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * A group of {@link RLibLocation R library locations}.
 */
@NonNullByDefault
public interface RLibGroup {
	
	/** Id of R default library group */
	String R_DEFAULT= "r.default"; //$NON-NLS-1$
	/** Id of R site libraries group */
	String R_SITE= "r.site"; //$NON-NLS-1$
	/** Id of R user libraries group */
	String R_USER= "r.user"; //$NON-NLS-1$
	/** Id of R other libraries group */
	String R_OTHER= "r.common"; //$NON-NLS-1$
	
	
	String getId();
	
	String getLabel();
	
	List<? extends RLibLocation> getLibLocations();
	
}
