/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.nio.file.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicRLibLocation implements RLibLocation {
	
	
	private final String source;
	
	private final @Nullable String label;
	
	private final String directory;
	protected @Nullable Path directoryPath;
	
	
	public BasicRLibLocation(final String source, final String directory,
			final @Nullable String label) {
		this.source= nonNullAssert(source);
		this.directory= nonNullAssert(directory);
		this.label= label;
	}
	
	public BasicRLibLocation(final RLibLocation template) {
		this(template.getSource(), template.getDirectory(), template.getLabel());
	}
	
	
	@Override
	public String getSource() {
		return this.source;
	}
	
	@Override
	public @Nullable String getLabel() {
		return this.label;
	}
	
	
	@Override
	public String getDirectory() {
		return this.directory;
	}
	
	@Override
	public @Nullable Path getDirectoryPath() {
		return this.directoryPath;
	}
	
	protected void setDirectoryPath(final @Nullable Path directoryPath) {
		this.directoryPath= directoryPath;
	}
	
	
	@Override
	public int hashCode() {
		return this.directory.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BasicRLibLocation) {
			final BasicRLibLocation other= (BasicRLibLocation) obj;
			return this.directory.equals(other.getDirectory());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.directory;
	}
	
}
