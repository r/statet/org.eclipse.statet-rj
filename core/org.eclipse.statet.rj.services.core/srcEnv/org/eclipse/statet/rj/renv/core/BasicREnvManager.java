/*=============================================================================#
 # Copyright (c) 2018, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicREnvManager implements REnvManager {
	
	
	protected static class ActualREnv extends REnv {
		
		private String name;
		
		private boolean isDeleted;
		
		private REnvConfiguration config;
		
		
		public ActualREnv(final String id) {
			super(id);
		}
		
		
		@Override
		public String getName() {
			return this.name;
		}
		
		@Override
		public REnv resolve() {
			return this;
		}
		
		@Override
		public boolean isDeleted() {
			return this.isDeleted;
		}
		
		protected REnvConfiguration getConfiguration() {
			return this.config;
		}
		
		@Override
		@SuppressWarnings("unchecked")
		public <T> @Nullable T get(final Class<T> type) {
			if (type == REnvConfiguration.class) {
				return (T) getConfiguration();
			}
			return null;
		}
		
	}
	
	private static final Pattern ID_PATTERN= Pattern.compile("[\\w_.]+");
	
	
	private ImList<ActualREnv> list= ImCollections.emptyList();
	
	
	public BasicREnvManager() {
	}
	
	
	@Override
	public ImList<ActualREnv> list() {
		return this.list;
	}
	
	@Override
	public @Nullable REnv get(final @Nullable String id, final @Nullable String name) {
		final ImList<? extends REnv> list= this.list;
		if (id != null) {
			for (final REnv rEnv : list) {
				if (rEnv.getId().equals(id)) {
					return rEnv;
				}
			}
		}
		if (name != null) {
			for (final REnv rEnv : list) {
				if (rEnv.getName().equals(name)) {
					return rEnv;
				}
			}
		}
		return null;
	}
	
	
	@Override
	public void add(final REnvConfiguration rEnvConfig) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void delete(final REnv rEnv) {
		throw new UnsupportedOperationException();
	}
	
	
	protected boolean isValidId(final String id) {
		return ID_PATTERN.matcher(id).matches();
	}
	
	protected ActualREnv newEnv(final String id) {
		if (!isValidId(id)) {
			throw new IllegalArgumentException("id=" + id);
		}
		return new ActualREnv(id.intern());
	}
	
	protected void setActualEnvs(final ImList<ActualREnv> list) {
		this.list= list;
	}
	
	protected void updateEnv(final ActualREnv rEnv, final @Nullable REnvConfiguration config) {
		if (config != null) {
			rEnv.name= config.getName();
			rEnv.config= config;
		}
		else {
			rEnv.isDeleted= true;
		}
	}
	
	protected void addEnv(final REnvConfiguration config) {
		final ActualREnv rEnv= (ActualREnv)config.getREnv();
		updateEnv(rEnv, config);
		ImList<ActualREnv> list= this.list;
		final int idx= list.indexOf(rEnv);
		if (idx < 0) {
			list= ImCollections.addElement(list, rEnv);
			setActualEnvs(list);
		}
	}
	
}
