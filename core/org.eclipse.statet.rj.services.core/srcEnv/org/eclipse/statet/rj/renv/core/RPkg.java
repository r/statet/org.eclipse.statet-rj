/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * R package.
 * 
 * Basic interface for representation of an R package.
 */
@NonNullByDefault
public interface RPkg {
	
	
	/**
	 * The name of the package.
	 * 
	 * @return the name
	 */
	String getName();
	
	/**
	 * The version of the package.
	 * 
	 * @return the version
	 */
	RNumVersion getVersion();
	
}
