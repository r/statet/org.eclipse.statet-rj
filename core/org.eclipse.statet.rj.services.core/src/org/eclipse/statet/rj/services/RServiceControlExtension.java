/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;


/**
 * 
 * @provisional
 */
@NonNullByDefault
public interface RServiceControlExtension {
	
	
	/**
	 * Adds a cancel handler called when the tool is canceled to the stack of cancel handlers.
	 * <p>
	 * The cancel handler should return <code>true</code> if the cancel event was handled
	 * completely and the other handlers will not be called.</p>
	 * 
	 * @param handler the handler
	 */
	void addCancelHandler(final Callable<Boolean> handler);
	
	/**
	 * Removes a cancel handler from the stack of cancel handlers.
	 * 
	 * @param handler the handler
	 */
	void removeCancelHandler(final Callable<Boolean> handler);
	
	
	/**
	 * The lock for wait operations.
	 * 
	 * @return the lock
	 */
	Lock getWaitLock();
	
	/**
	 * Waits in the current tool thread.
	 * <p>
	 * If short background operations are waiting for execution, they are executed (depends on
	 * implementation).</p>
	 * <p>
	 * The current thread must hold the lock {@link #getWaitLock()}. The method returns after a
	 * short waiting time, operations are executed <b>or</b> {@link #resume()} is called.</p>
	 * 
	 * @param m the current monitor
	 */
	void waitingForUser(final ProgressMonitor m);
	
	/**
	 * Resumes the tool thread which is waiting in {@link #waitingForUser(ProgressMonitor)}.
	 */
	void resume();
	
}
