/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Line with end points <code>(x0, y0)</code> and <code>(x1, y1)</code>.
 */
@NonNullByDefault
public class RLine extends RGraphicElement {
	
	
	public final double x0;
	public final double y0;
	public final double x1;
	public final double y1;
	
	
	/**
	 * Creates a new line
	 * 
	 * @param x0 x coordinate of point 1
	 * @param y0 y coordinate of point 1
	 * @param x1 x coordinate of point 2
	 * @param y1 y coordinate of point 2
	 */
	public RLine(final double x0, final double y0, final double x1, final double y1) {
		this.x0= x0;
		this.y0= y0;
		this.x1= x1;
		this.y1= y1;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return DRAW_LINE;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(50);
		sb.append("Line[(");
		sb.append(this.x0);
		sb.append(",");
		sb.append(this.y0);
		sb.append("), (");
		sb.append(this.x1);
		sb.append(",");
		sb.append(this.y1);
		sb.append(")]");
		return sb.toString();
	}
	
}
