/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.rcpdemo;

import org.osgi.framework.BundleContext;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.rj.eclient.graphics.comclient.ERGraphicFactory;
import org.eclipse.statet.rj.example.rcpdemo.RServiManager;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rj.example.rcpdemo2"; //$NON-NLS-1$
	
	
	private static Activator instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static Activator getInstance() {
		return instance;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return imageDescriptorFromPlugin(BUNDLE_ID, path);
	}
	
	
	private RServiManager rserviManager;
	
	private ERGraphicFactory graphicFactory;
	
	
	/**
	 * The constructor
	 */
	public Activator() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		Disposable disposable;
		synchronized (this) {
			disposable= this.rserviManager;
			this.rserviManager= null;
		}
		if (disposable != null) {
			disposable.dispose();
		}
		
		instance= null;
		super.stop(context);
	}
	
	
	public synchronized RServiManager getRServiManager() {
		if (this.rserviManager == null) {
			this.rserviManager= new RServiManager("RCPDemo", getRGraphicFactory());
		}
		return this.rserviManager;
	}
	
	public synchronized ERGraphicFactory getRGraphicFactory() {
		if (this.graphicFactory == null) {
			this.graphicFactory= new ERGraphicFactory();
		}
		return this.graphicFactory;
	}
	
}
