/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.example.rcpdemo;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.internal.rj.example.rcpdemo.Activator;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.services.RService;


public abstract class RJob extends Job {
	
	
	private final RServiManager rServiManager;
	
	
	public RJob(final String name) {
		super(name);
		this.rServiManager= Activator.getInstance().getRServiManager();
		setRule(this.rServiManager.getSchedulingRule());
	}
	
	
	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		final ProgressMonitor m= EStatusUtils.convert(monitor, 1);
		RServi servi= null;
		try {
			servi= this.rServiManager.getRServi(getName());
			runRTask(servi, m);
		}
		catch (final StatusException e) {
			return new Status(IStatus.ERROR, Activator.BUNDLE_ID,
					"An error occurred when running " + getName() + ".", e);
		}
		finally {
			if (servi != null) {
				try {
					servi.close();
				} catch (final StatusException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return Status.OK_STATUS;
	}
	
	@Override
	public boolean belongsTo(final Object family) {
		return this.rServiManager == family;
	}
	
	protected abstract void runRTask(RService r, ProgressMonitor m) throws StatusException;
	
}
