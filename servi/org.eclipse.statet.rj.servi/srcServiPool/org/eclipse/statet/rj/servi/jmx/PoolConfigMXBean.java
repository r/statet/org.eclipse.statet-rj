/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.jmx;

import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface PoolConfigMXBean {
	
	
	@DisplayName("Max total nodes (count)")
	int getMaxTotalCount();
	void setMaxTotalCount(int count);
	
	@DisplayName("Max idle nodes (count)")
	int getMaxIdleCount();
	void setMaxIdleCount(int count);
	
	@DisplayName("Min idle nodes (count)")
	int getMinIdleCount();
	void setMinIdleCount(int count);
	
	@DisplayName("Max reuse of node (count)")
	int getMaxUsageCount();
	void setMaxUsageCount(int count);
	
	@DisplayName("Min idle duration of node before automatic eviction (millisec)")
	long getAutoEvictionMinIdleTimeMillis();
	void setAutoEvictionMinIdleTimeMillis(long milliseconds);
	
	@DisplayName("Timeout when requesting node (millisec)")
	long getAllocationTimeoutMillis();
	void setAllocationTimeoutMillis(long milliseconds);
	
	@DisplayName("Timeout when evicting node in use (millisec)")
	long getEvictionTimeoutMillis();
	void setEvictionTimeoutMillis(long milliseconds);
	
	
	@DisplayName("Apply the current configuration")
	void apply() throws OperationsException;
	
	@DisplayName("Reset current changes / load actual configuration")
	void loadActual() throws OperationsException;
	@DisplayName("Load the default configuration")
	void loadDefault() throws OperationsException;
	@DisplayName("Load the saved configuration")
	void loadSaved() throws OperationsException;
	@DisplayName("Save the current configuration")
	void save() throws OperationsException;
	
}
