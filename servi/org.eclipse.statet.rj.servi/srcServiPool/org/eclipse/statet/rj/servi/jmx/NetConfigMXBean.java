/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.jmx;

import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface NetConfigMXBean {
	
	
	@DisplayName("Host address (IP/name)")
	@Nullable String getHostAddress();
	void setHostAddress(@Nullable String address);
	
	@DisplayName("RMI registry port (-1 \u21d2 default)")
	int getRegistryPort();
	void setRegistryPort(int port);
	
	@DisplayName("Start embedded registry")
	boolean getRegistryEmbed();
	void setRegistryEmbed(boolean embed);
	
	@DisplayName("Enable SSL")
	boolean isSSLEnabled();
	void setSSLEnabled(boolean enable);
	
	
	@DisplayName("Load the default configuration")
	void loadDefault() throws OperationsException;
	@DisplayName("Load the saved configuration")
	void loadSaved() throws OperationsException;
	@DisplayName("Save the current configuration")
	void save() throws OperationsException;
	
}
