/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.time.Instant;

import javax.management.JMException;
import javax.management.ObjectName;
import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIAddress;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.jmx.NodeMXBean;
import org.eclipse.statet.rj.servi.jmx.NodeStateMX;
import org.eclipse.statet.rj.servi.pool.PoolConfig;
import org.eclipse.statet.rj.servi.pool.PoolNodeItem;
import org.eclipse.statet.rj.servi.pool.PoolNodeObject;
import org.eclipse.statet.rj.servi.pool.PoolServer;


@NonNullByDefault
public class MXNode implements NodeMXBean {
	
	
	private final PoolServer server;
	private final PoolNodeObject nodeObj;
	private final RMIAddress address;
	private final Instant creationTime;
	
	private @Nullable ObjectName jmName;
	
	
	public MXNode(final PoolServer server, final PoolNodeObject nodeObj) {
		this.server= server;
		this.nodeObj= nodeObj;
		final NodeHandler nodeHandler= nonNullAssert(this.nodeObj.getNodeHandler());
		this.address= nonNullAssert(nodeHandler.getAddress());
		this.creationTime= this.nodeObj.getCreationTime();
	}
	
	
	public void initJM() throws JMException {
		final ObjectName jmName= new ObjectName(this.server.getJMBaseName() + "type=Server.Node,rservi.nodeId=" + this.address.getName());
		this.jmName= jmName;
		ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmName);
	}
	
	public void disposeJM() throws JMException {
		final ObjectName jmName= this.jmName;
		if (jmName != null) {
			this.jmName= null;
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmName);
		}
	}
	
	
	@Override
	public String getId() {
		return this.address.getName();
	}
	
	@Override
	public Instant getCreationTime() {
		return this.creationTime;
	}
	
	@Override
	public NodeStateMX getState() {
		final PoolNodeItem item= new PoolNodeItem(this.nodeObj, Instant.now());
		return new MXNodeState(item);
	}
	
	@Override
	public boolean isConsoleEnabled() {
		final NodeHandler nodeHandler= this.nodeObj.getNodeHandler();
		return (nodeHandler != null && nodeHandler.isConsoleEnabled());
	}
	
	@Override
	public synchronized void setConsoleEnabled(final boolean enable) throws OperationsException {
		try {
			final NodeHandler nodeHandler= this.nodeObj.getNodeHandler();
			if (nodeHandler == null) {
				throw new UnsupportedOperationException("not available");
			}
			if (enable) {
				nodeHandler.enableConsole("none");
			}
			else {
				nodeHandler.disableConsole();
			}
		}
		catch (final RjException e) {
			throw new OperationsException(e.getMessage());
		}
	}
	
	@Override
	public void stop() throws OperationsException {
		final PoolConfig config= new PoolConfig();
		this.server.getPoolConfig(config);
		this.nodeObj.evict(config.getEvictionTimeout());
	}
	
	@Override
	public void stop(final long timeoutMillis) throws OperationsException {
		if (timeoutMillis < 0) {
			throw new OperationsException("Invalid parameter 'timeoutMillis' >= 0.");
		}
		this.nodeObj.evict(Duration.ofMillis(timeoutMillis));
	}
	
	@Override
	public void kill() throws OperationsException {
		this.nodeObj.evict(null);
	}
	
}
