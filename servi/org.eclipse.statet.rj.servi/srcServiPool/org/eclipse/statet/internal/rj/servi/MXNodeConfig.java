/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.util.Map;

import javax.management.JMException;
import javax.management.ObjectName;
import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.servi.jmx.NodeConfigMXBean;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;
import org.eclipse.statet.rj.servi.pool.PoolServer;


@NonNullByDefault
public class MXNodeConfig extends RServiNodeConfig implements NodeConfigMXBean {
	
	
	private final PoolServer server;
	
	private @Nullable ObjectName jmName;
	
	
	public MXNodeConfig(final PoolServer server) throws JMException {
		this.server= nonNullAssert(server);
	}
	
	
	public void initJM() throws JMException {
		final ObjectName jmName= new ObjectName(this.server.getJMBaseName() + "type=Server.NodeConfig");
		this.jmName= jmName;
		ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmName);
	}
	
	public void disposeJM() throws JMException {
		final ObjectName jmName= this.jmName;
		if (jmName != null) {
			this.jmName= null;
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmName);
		}
	}
	
	
	@Override
	public synchronized void setEnvironmentVariable(final String name, final @Nullable String value) {
		final Map<String, String> variables= getEnvironmentVariables();
		if (value == null) {
			variables.remove(name);
		}
		else {
			variables.put(name, value);
		}
	}
	
	
	@Override
	public long getStartStopTimeoutMillis() {
		final var timeout= getStartStopTimeout();
		return (timeout != null) ? timeout.toMillis() : -1;
	}
	
	@Override
	public void setStartStopTimeoutMillis(final long milliseconds) {
		final var duration= (milliseconds != -1) ? Duration.ofMillis(milliseconds) : null;
		setStartStopTimeout(duration);
	}
	
	
	@Override
	public void apply() throws OperationsException {
		synchronized (this) {
			MXUtils.validate(this);
			
			try {
				this.server.setNodeConfig(this);
			}
			catch (final RjInvalidConfigurationException e) {
				throw new OperationsException(e.getMessage());
			}
		}
	}
	
	@Override
	public void loadActual() throws OperationsException {
		this.server.getNodeConfig(this);
	}
	
	@Override
	public void loadDefault() throws OperationsException {
		load(new RServiNodeConfig());
	}
	
	@Override
	public void loadSaved() throws OperationsException {
		MXUtils.load(this, this.server.getRJContext());
	}
	
	@Override
	public void save() throws OperationsException {
		MXUtils.save(this, this.server.getRJContext());
	}
	
}
