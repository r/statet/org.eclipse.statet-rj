/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.lang.management.ManagementFactory;
import java.time.Instant;
import java.util.Date;

import javax.management.JMException;
import javax.management.ObjectName;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.servi.jmx.NodeStateMX;
import org.eclipse.statet.rj.servi.jmx.PoolStatusMX;
import org.eclipse.statet.rj.servi.pool.PoolNodeItem;
import org.eclipse.statet.rj.servi.pool.PoolServer;
import org.eclipse.statet.rj.servi.pool.PoolStatus;


@NonNullByDefault
public class MXPoolStatus extends PoolStatus<NodeStateMX> implements PoolStatusMX {
	
	
	private @Nullable ObjectName jmName;
	
	private Instant time= nonNullLateInit(); // ::refresh
	
	
	public MXPoolStatus(final PoolServer server) {
		super(server);
		
		refresh();
	}
	
	
	public void initJM() throws JMException {
		final ObjectName jmName= new ObjectName(this.server.getJMBaseName() + "type=Server.PoolStatus");
		this.jmName= jmName;
		ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmName);
	}
	
	public void disposeJM() throws JMException {
		final ObjectName jmName= this.jmName;
		if (jmName != null) {
			this.jmName= null;
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmName);
		}
	}
	
	protected synchronized void refresh() {
		final Instant stamp= Instant.now();
		refresh(this.server.getManager(), stamp);
		this.time= stamp;
	}
	
	
	@Override
	protected NodeStateMX createNodeState(final PoolNodeItem item) {
		return new MXNodeState(item);
	}
	
	
	@Override
	public synchronized Date getStatusTime() {
		return Date.from(this.time);
	}
	
	
}
