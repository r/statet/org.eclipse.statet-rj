/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.node.RServiNodeFactory;


@NonNullByDefault
public interface NodeFactory extends RServiNodeFactory {
	
	
	void createNode(NodeHandler handler) throws RjException;
	void stopNode(NodeHandler handler);
	
}
