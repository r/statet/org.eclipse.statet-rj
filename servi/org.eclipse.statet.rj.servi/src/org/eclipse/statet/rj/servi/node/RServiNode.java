/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.node;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rj.servi.server.RServiBackend;
import org.eclipse.statet.rj.RjException;


@NonNullByDefault
public interface RServiNode extends Remote {
	
	
	void ping() throws RemoteException;
	
	String getPoolHost() throws RemoteException;
	RServiBackend bindClient(String client) throws RemoteException;
	void unbindClient() throws RemoteException;
	
	void shutdown() throws RemoteException;
	
	int getEvalTime() throws RemoteException;
	
	boolean setConsole(@Nullable String authConfig) throws RjException, RemoteException;
	
	/**
	 * Runs the given code in R
	 * 
	 * @param code the R code
	 * @throws RjException if an R error occurred when running the snippet
	 * @throws RemoteException if an RMI/communication error occurred
	 */
	void runSnippet(String code) throws RjException, RemoteException;
	
}
