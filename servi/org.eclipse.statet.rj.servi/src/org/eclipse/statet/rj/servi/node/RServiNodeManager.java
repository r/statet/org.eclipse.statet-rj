/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.node;

import java.util.NoSuchElementException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.RServi;


/**
 * Manager for a directly controlled RServi instance.
 * 
 * @see RServiImpl
 */
@NonNullByDefault
public interface RServiNodeManager {
	
	
	String getId();
	
	/**
	 * Returns the R factory passed during creation.
	 * 
	 * @return the factory
	 */
	RServiNodeFactory getFactory();
	
	/**
	 * Starts the RServi respectively R instance.
	 * Does nothing if already started.
	 * 
	 * @throws RjException if the node could not be started
	 */
	void start() throws RjException;
	
	/**
	 * Stops the RServi respectively R instance.
	 * Does nothing if not started.
	 */
	void stop();
	
	/**
	 * Returns the RServi instance.
	 * 
	 * @param name
	 * @return the RServi instance
	 * @throws NoSuchElementException if the RServi instance is already in use
	 * @throws RjException if an error occurred
	 */
	RServi getRServi(final String name) throws NoSuchElementException, RjException;
	
}
