/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.security.auth.login.LoginException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.rmi.RMIRegistry;
import org.eclipse.statet.jcommons.rmi.RMIRegistryManager;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.server.util.RJContext;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.RServiUtils;
import org.eclipse.statet.rj.servi.node.RServiImpl;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;
import org.eclipse.statet.rj.servi.node.RServiNodeFactory;
import org.eclipse.statet.rj.servi.node.RServiNodeManager;


@NonNullByDefault
public abstract class AbstractLocalNodeTest extends AbstractServiTest {
	
	
	private final String name;
	
	protected RServiNodeManager localR= nonNullLateInit();
	
	
	public AbstractLocalNodeTest() throws Exception {
		super();
		this.name= getClass().getSimpleName();
	}
	
	
	@BeforeEach
	public void initNode() throws Exception {
		final RJContext context= ServiTests.getRJContext();
		final RMIRegistry rmiRegistry= RMIRegistryManager.INSTANCE.getEmbeddedPrivateRegistry(
				new NullProgressMonitor() );
		
		final RServiNodeConfig nodeConfig= new RServiNodeConfig();
		nodeConfig.load(getEnvConfiguration("default"));
		
		final RServiNodeFactory nodeFactory= RServiImpl.createLocalNodeFactory(this.name, context);
		nodeFactory.setRegistry(rmiRegistry);
		nodeFactory.setConfig(nodeConfig);
		
		this.localR= nonNullAssert(
				RServiImpl.createNodeManager(this.name, rmiRegistry, nodeFactory) );
	}
	
	@AfterEach
	public void dispose() throws Exception {
		final List<Throwable> exceptions= new ArrayList<>();
		disposeServis(exceptions);
		
		final var localR= this.localR;
		if (localR != null) {
			try {
				localR.stop();
			}
			catch (final Exception e) {
				exceptions.add(e);
			}
			finally {
				this.localR= nonNullLateInit();
			}
		}
		
		reportErrors(exceptions);
	}
	
	
	protected RServi getServi(final String id)
			throws NoSuchElementException, LoginException, StatusException {
		final RServi servi= RServiUtils.getRServi(this.localR, id);
		onServiGet(servi);
		return servi;
	}
	
}
