/*=============================================================================#
 # Copyright (c) 2020, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.tests;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.pool.AbstractLocalNodeTest;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class RServiceTest extends AbstractLocalNodeTest {
	
	
	private static Path tempDir= nonNullLateInit();
	
	
	public RServiceTest() throws Exception {
	}
	
	
	@BeforeAll
	public static void initTemp() throws IOException {
		tempDir= Files.createTempDirectory("RServiceTest");
	}
	
	
	@BeforeEach
	@Override
	public void initNode() throws Exception {
		super.initNode();
		this.localR.start();
	}
	
	
	@Test
	public void uploadFile_Stream() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RServi servi= getServi("uploadFile_Stream");
		
		final byte[] content= "Hello World!".getBytes(StandardCharsets.UTF_8);
		RObject rContentObj;
		
		servi.uploadFile(new ByteArrayInputStream(content), content.length, "relative.txt", 0, m);
		rContentObj= servi.createFunctionCall("readBin")
				.addChar("con", "relative.txt")
				.addInt("n", 100)
				.addChar("what", "raw")
				.evalData(m);
		assertArrayEquals(content, RDataUtils.checkRRawVector(rContentObj).getData().toRawArray());
		
		final var rwd= Path.of(RDataUtils.checkSingleCharValue(
				servi.createFunctionCall("getwd").evalData(m) ));
		servi.uploadFile(new ByteArrayInputStream(content), content.length,
				rwd.resolve("absolute.txt").toString(), 0, m );
		rContentObj= servi.createFunctionCall("readBin")
				.addChar("con", "absolute.txt")
				.addInt("n", 100)
				.addChar("what", "raw")
				.evalData(m);
		assertArrayEquals(content, RDataUtils.checkRRawVector(rContentObj).getData().toRawArray());
		
		assertThrows(StatusException.class, () -> {
			servi.uploadFile(new ByteArrayInputStream(content), content.length,
					"invalid://text.txt", 0, m );
		});
		
		assertThrows(StatusException.class, () -> {
			servi.uploadFile(new ByteArrayInputStream(content), content.length + 1,
					"toosmall.txt", 0, m );
		});
		
		closeServi(servi);
	}
	
	@Test
	public void uploadFile_Example1() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RServi servi= getServi("uploadFile_Example1");
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		final Path testFile= tempDir.resolve("uploadFile_Example1.txt");
		
		Files.write(testFile, content);
		try (final var in= Files.newInputStream(testFile)) {
			servi.uploadFile(in, Files.size(testFile), "test.txt", 0, m);
		}
		final var rContent= servi.createFunctionCall("readBin")
				.addChar("con", "test.txt")
				.addInt("n", 100)
				.addChar("what", "raw")
				.evalData(m);
		closeServi(servi);
		
		assertArrayEquals(content, RDataUtils.checkRRawVector(rContent).getData().toRawArray());
	}
	
	@Test
	public void downloadFile_Stream() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RServi servi= getServi("downloadFile_Stream");
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		final ByteArrayOutputStream out= new ByteArrayOutputStream();
		
		servi.createFunctionCall("writeBin")
				.add(DefaultRObjectFactory.INSTANCE.createRawVector(content))
				.addChar("con", "relative.txt").addLogi("useBytes", true)
				.evalVoid(m);
		out.reset();
		servi.downloadFile(out, "relative.txt", 0, m);
		assertArrayEquals(content, out.toByteArray());
		
		final var rwd= Path.of(RDataUtils.checkSingleCharValue(
				servi.createFunctionCall("getwd").evalData(m) ));
		servi.createFunctionCall("writeBin")
				.add(DefaultRObjectFactory.INSTANCE.createRawVector(content))
				.addChar("con", "absolute.txt").addLogi("useBytes", true)
				.evalVoid(m);
		out.reset();
		servi.downloadFile(out, rwd.resolve("absolute.txt").toString(), 0, m);
		assertArrayEquals(content, out.toByteArray());
		
		assertThrows(StatusException.class, () -> {
			out.reset();
			servi.downloadFile(out, "invalid://text.txt", 0, m);
		});
		
		assertThrows(StatusException.class, () -> {
			out.reset();
			servi.downloadFile(out, "missing.txt", 0, m);
		});
		
		closeServi(servi);
	}
	
	@Test
	public void downloadFile_Array() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RServi servi= getServi("downloadFile_Stream");
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		byte[] rContent;
		
		servi.createFunctionCall("writeBin")
				.add(DefaultRObjectFactory.INSTANCE.createRawVector(content))
				.addChar("con", "relative.txt").addLogi("useBytes", true)
				.evalVoid(m);
		rContent= servi.downloadFile("relative.txt", 0, m);
		assertArrayEquals(content, rContent);
		
		final var rwd= Path.of(RDataUtils.checkSingleCharValue(
				servi.createFunctionCall("getwd").evalData(m) ));
		servi.createFunctionCall("writeBin")
				.add(DefaultRObjectFactory.INSTANCE.createRawVector(content))
				.addChar("con", "absolute.txt").addLogi("useBytes", true)
				.evalVoid(m);
		rContent= servi.downloadFile(rwd.resolve("absolute.txt").toString(), 0, m);
		assertArrayEquals(content, rContent);
		
		assertThrows(StatusException.class, () -> {
			servi.downloadFile("invalid://text.txt", 0, m);
		});
		
		assertThrows(StatusException.class, () -> {
			servi.downloadFile("missing.txt", 0, m);
		});
		
		closeServi(servi);
	}
	
	@Test
	public void downloadFile_Example1() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RServi servi= getServi("downloadFile_Example1");
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		final Path testFile= tempDir.resolve("downloadFile_Example1.txt");
		
		servi.createFunctionCall("writeBin")
				.add(DefaultRObjectFactory.INSTANCE.createRawVector(content))
				.addChar("con", "test.txt").addLogi("useBytes", true)
				.evalVoid(m);
		try (final var out= Files.newOutputStream(testFile)) {
			servi.downloadFile(out, "test.txt", 0, m);
		}
		closeServi(servi);
		final var fileText= Files.readAllBytes(testFile);
		
		assertArrayEquals(content, fileText);
	}
	
}
