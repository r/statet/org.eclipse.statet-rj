/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.eclipse.statet.rj.servi.pool.PoolConfig;
import org.eclipse.statet.rj.servi.pool.PoolServer;


public class PoolConfigBean extends PoolConfig {
	
	
	public PoolConfigBean() {
	}
	
	
	@PostConstruct
	public void init() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		poolServer.getPoolConfig(this);
	}
	
	
	public String actionLoadCurrent() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		synchronized (this) {
			poolServer.getPoolConfig(this);
			FacesUtils.validate(this);
		}
		
		return RJWeb.POOLCONFIG_NAV;
	}
	
	public String actionLoadDefaults() {
		synchronized (this) {
			load(new PoolConfig());
			FacesUtils.validate(this);
		}
		
		return RJWeb.POOLCONFIG_NAV;
	}
	
	public String actionApply() {
		final PoolConfig config= new PoolConfig(this);
		
		if (!FacesUtils.validate(config)) {
			return null;
		}
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		poolServer.setPoolConfig(config);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuration applied.", null));
		return null;
	}
	
	public String actionSaveAndApply() {
		final PoolConfig config= new PoolConfig(this);
		
		if (!FacesUtils.validate(config)) {
			return null;
		}
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		poolServer.setPoolConfig(config);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuration applied.", null));
		
		FacesUtils.saveToFile(config);
		return null;
	}
	
}
