/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.time.Instant;

import javax.annotation.PostConstruct;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.servi.pool.PoolNodeItem;
import org.eclipse.statet.rj.servi.pool.PoolNodeObject;
import org.eclipse.statet.rj.servi.pool.PoolStatus;
import org.eclipse.statet.rj.servi.pool.RServiPoolManager;


@NonNullByDefault
public class PoolStatusBean extends PoolStatus<PoolItemBean> {
	
	
	private boolean forceRefresh;
	private boolean autoRefresh;
	
	
	public PoolStatusBean() {
		super(FacesUtils.getPoolServer());
	}
	
	
	@PostConstruct
	public void init() {
		load();
	}
	
	private void load() {
		final Instant stamp= Instant.now();
		final RServiPoolManager poolManager= this.server.getManager();
		if (poolManager == null) {
			FacesUtils.addErrorMessage(null, "The pool is currently not available.");
		}
		refresh(poolManager, stamp);
	}
	
	@Override
	protected PoolNodeItem createPoolItem(final PoolNodeObject itemData, final Instant stamp) {
		return new PoolItemBean(itemData, stamp);
	}
	
	@Override
	protected PoolItemBean createNodeState(final PoolNodeItem item) {
		return (PoolItemBean) item;
	}
	
	
	public synchronized Instant getStamp() {
		check();
		return super.getStatusStamp();
	}
	
	@Override
	protected void check() {
		if (this.forceRefresh) {
			load();
		}
	}
	
	public synchronized void forceRefresh() {
		this.forceRefresh= true;
	}
	
	
	public @Nullable String actionRefresh() {
		return null;
	}
	
	public synchronized @Nullable String actionEnableAutoRefresh() {
		this.autoRefresh= true;
		return null;
	}
	
	public synchronized @Nullable String actionDisableAutoRefresh() {
		this.autoRefresh= false;
		return null;
	}
	
	public synchronized void setAutoRefreshEnabled(final boolean enabled) {
		this.autoRefresh= enabled;
	}
	
	public synchronized boolean isAutoRefreshEnabled() {
		return this.autoRefresh;
	}
	
}
